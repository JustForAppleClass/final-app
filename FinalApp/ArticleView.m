//
//  ArticleView.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ArticleView.h"

@implementation ArticleView

-(void)viewDidLoad
{
    self.navigationItem.title = @"News";
    self.view.backgroundColor = [UIColor MyYellow];
    
    //Set up temporary Headlines, bylines, and abstract
    //Headline
    UILabel* headline = [UILabel new];
    headline.backgroundColor = [UIColor MyGreen];
    [headline setTranslatesAutoresizingMaskIntoConstraints:NO];
    headline.text = @"Something something tequila";
    headline.font = [UIFont fontWithName:@"Avenir Next" size:25 ];
    headline.textColor = [UIColor whiteColor];
    [self.view addSubview:headline];
    
    //Byline
    UILabel* byline = [UILabel new];
    byline.backgroundColor = [UIColor MyRed];
    [byline setTranslatesAutoresizingMaskIntoConstraints:NO];
    byline.text = @"An article by Jimmy Buffet";
    byline.font = [UIFont fontWithName:@"Avenir Next" size:20 ];
    byline.textColor = [UIColor whiteColor];
    [self.view addSubview:byline];
    
    //Publication date
    UILabel* date = [UILabel new];
    date.backgroundColor = [UIColor MyBlue];
    date.text = @"9/23/15";
    [date setTranslatesAutoresizingMaskIntoConstraints:NO];
    date.font = [UIFont fontWithName:@"Avenir Next" size:20];
    date.textColor = [UIColor whiteColor];
    [self.view addSubview:date];
    
    //Abstract
    UITextView* abstract = [UITextView new];
    [abstract setTranslatesAutoresizingMaskIntoConstraints:NO];
    abstract.text = @"Something something tequila, the story of a man full of very little other than tequila or regret.My reaction through-out the video:Ok those are eye balls.Everything seems fine so far.I wonder what Beethoven would think of this generation if we brought him back from the dead";
    abstract.backgroundColor = [UIColor MyLavender];
    
    [self.view addSubview:abstract];
    
    //Create a small size for title elements
    NSNumber* small = [NSNumber numberWithDouble:self.view.frame.size.height/10];
    NSNumber* small_2 = [NSNumber numberWithDouble:self.view.frame.size.height/20];
    
    
    //Create dictionaries for autolayout
    NSDictionary *views = @{
                            @"Head":headline,
                            @"by":byline,
                            @"date":date,
                            @"abstract":abstract,
                            };
    
    NSDictionary* metrics = @{
                              @"small":small,
                              @"small_2":small_2,
                              };
    
    //Create and add autolayout constraints
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[Head(small)][by(small_2)][date(small_2)][abstract]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[Head]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[by]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[date]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[abstract]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];

}

@end
