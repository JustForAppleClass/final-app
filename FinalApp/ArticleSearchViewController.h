//
//  ArticleSearchViewController.h
//  FinalApp
//
//  Created by Michelle Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+MyRed.h"
#import "ArticleCell.h"
#import "ArticleView.h"

@interface ArticleSearchViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* newArticles;
}


@end
