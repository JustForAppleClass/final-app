//
//  MovieInfoViewController.h
//  FinalApp
//
//  Created by Michelle Griffin on 7/23/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+MyRed.h"

@interface MovieInfoViewController : UIViewController

@property(strong,nonatomic)UILabel* movieTitle;
@property(strong,nonatomic)UILabel* releaseDate;
@property(strong,nonatomic)UILabel* criticStars;
@property(strong, nonatomic)UITextView* plot;

@end
