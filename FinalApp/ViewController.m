//
//  ViewController.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/15/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Menu";
    
    self.navigationController.navigationBar.barTintColor = [UIColor MyYellow];
    self.navigationController.navigationBar.translucent = NO;
    
    UIButton* settings = [UIButton buttonWithType:UIButtonTypeSystem];
    settings.frame = CGRectMake(0, 0, 100, 20);
    
    
    [settings setTitle:@"Settings" forState:UIControlStateNormal];
    
    [settings addTarget:self action:@selector(goToSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* settingButton = [[UIBarButtonItem alloc]initWithCustomView:settings];
    
    self.navigationItem.rightBarButtonItem = settingButton;
    
    
    //Set up a background frame to hold nearly everything
    self.background = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height)];
    self.background.backgroundColor = [UIColor MyGreen];
    [self.view addSubview:self.background];

    //Set up vertical street/add it to view
    UIImageView* street = [[UIImageView alloc]initWithFrame:CGRectMake(self.background.frame.size.width/8*3, 0, self.background.frame.size.width/4, self.background.frame.size.height)];
    [street setImage:[UIImage imageNamed:@"IosRoad"]];
    [self.background addSubview:street];
    
    //Vertical position for the horizontal street and intersection
    double crossStreetHeight = self.background.frame.size.height/2 - self.background.frame.size.width/8;
    
    //Horizontal position for the intersection
    double intersectionHorPos = self.background.frame.size.width/2 - self.background.frame.size.width/8;
    
    //set up horizontal street/add it to view
    UIImageView* crossStreet = [[UIImageView alloc]init];
    [crossStreet setImage:[UIImage imageNamed:@"IosRoad"]];
    crossStreet.transform = CGAffineTransformMakeRotation(M_PI_2);
    crossStreet.frame = CGRectMake(0, crossStreetHeight, self.background.frame.size.height, self.background.frame.size.width/4);
    [self.background addSubview:crossStreet];
    
    //set up intersection/add it to view
    UIImageView* intersection = [[UIImageView alloc]initWithFrame:CGRectMake(intersectionHorPos, crossStreetHeight, self.background.frame.size.width/4, self.background.frame.size.width/4)];
    [intersection setImage:[UIImage imageNamed:@"IosIntersection"]];
    [self.background addSubview:intersection];
    
    //Begin set up of Gesture recieving object truckMover
    self.truckMover = [[TruckMover alloc]init];
    self.truckMover.frame = self.view.bounds;
    self.truckMover.self.backgroundColor = [UIColor clearColor];
    
    
    
    //Set up top truck
    self.truckMover.topTruck = [[Truck alloc]init];
    [self.background addSubview:self.truckMover.topTruck];
    
    //Set up top truck label
    self.truckMover.topTruck.txt.frame = CGRectMake(0, 0, 100, 30);
    self.truckMover.topTruck.txt.backgroundColor = [UIColor brownColor];
    self.truckMover.topTruck.txt.foreground.backgroundColor = [UIColor MyYellow];
    
    [self.view addSubview:self.truckMover.topTruck.txt];
    
    //Set up bottom truck
    self.truckMover.bottomTruck = [[Truck alloc]init];
    self.truckMover.bottomTruck.transform = CGAffineTransformMakeRotation(M_PI);
    [self.background addSubview:self.truckMover.bottomTruck];
    
    //Set up botton truck label
    self.truckMover.bottomTruck.txt.frame = CGRectMake(self.view.frame.size.width -100, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-42, 100, 30);
    self.truckMover.bottomTruck.txt.backgroundColor = [UIColor brownColor];
    self.truckMover.bottomTruck.txt.foreground.backgroundColor = [UIColor MyYellow];
    [self.view addSubview:self.truckMover.bottomTruck.txt];
    
    //Set up left truck
    self.truckMover.leftTruck = [[Truck alloc]init];
    self.truckMover.leftTruck.transform = CGAffineTransformMakeRotation(M_PI_2*3);
    
    [self.background addSubview:self.truckMover.leftTruck];
    
    //Set up left truck label
    self.truckMover.leftTruck.txt.frame = CGRectMake(0, self.background.frame.size.height/2 + self.view.frame.size.width/8, 100, 30);
    self.truckMover.leftTruck.txt.backgroundColor = [UIColor brownColor];
    self.truckMover.leftTruck.txt.foreground.backgroundColor = [UIColor MyYellow];
    [self.view addSubview:self.truckMover.leftTruck.txt];
    
    //Set up right truck
    self.truckMover.rightTruck = [[Truck alloc]init];
    self.truckMover.rightTruck.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    [self.background addSubview:self.truckMover.rightTruck];
    
    //Set up right truck label
    self.truckMover.rightTruck.txt.frame = CGRectMake(self.background.frame.size.width-100, self.background.frame.size.height/2 - self.view.frame.size.width/8-30, 100, 30);
    self.truckMover.rightTruck.txt.backgroundColor = [UIColor brownColor];
    self.truckMover.rightTruck.txt.foreground.backgroundColor = [UIColor MyYellow];
    [self.view addSubview:self.truckMover.rightTruck.txt];
    
    //Put the Truck Mover on top
    [self.view addSubview:self.truckMover];
    
    
    
    
    //Add down swipe gesture recognizer
    UISwipeGestureRecognizer* sgr = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeDown:)];
    [sgr setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.truckMover addGestureRecognizer:sgr];
    
    
    
    //Add up swipe gesture recognizer
    UISwipeGestureRecognizer* usgr = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeUp:)];
    [usgr setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.truckMover addGestureRecognizer:usgr];
    
    
    //Add right swipe gesture recognizer
    UISwipeGestureRecognizer* rsgr = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
    [rsgr setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.truckMover addGestureRecognizer:rsgr];
    
    //Add left swipe gesture recognizer
    UISwipeGestureRecognizer* lsgr = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
    [lsgr setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.truckMover addGestureRecognizer:lsgr];

}

-(void)viewWillAppear:(BOOL)animated
{
    //Call super function
    [super viewWillAppear:animated];
    
    //Make sure that user has control over the trucks
    [self.view addSubview:self.truckMover];
    
    /*Set the text label for each truck*/
    
    //Top truck text
    self.truckMover.topTruck.txt.foreground.text = @"Weather";
    
    //Bottom Truck text
    self.truckMover.bottomTruck.txt.foreground.text = @"News";
    
    //Left truck text
    self.truckMover.leftTruck.txt.foreground.text = @"Bills";
    
    //Right truck text
    self.truckMover.rightTruck.txt.foreground.text = @"Movies";
    
    /*Reset Truck positions*/
    
    //Reset top truck
    [self.truckMover.topTruck setFrame:CGRectMake(self.background.frame.size.width/8*3 +5, 0, self.background.frame.size.width/8-7, self.background.frame.size.width*100/375)];
    
    //Reset bottom truck
    [self.truckMover.bottomTruck setFrame:CGRectMake(self.background.frame.size.width/2 +5, self.background.frame.size.height-self.background.frame.size.width*100/375, self.background.frame.size.width/8-7, self.background.frame.size.width*100/375)];
    
    //Reset left truck
    [self.truckMover.leftTruck setFrame:CGRectMake(0, self.background.frame.size.height/2+5, self.background.frame.size.width*100/375, self.background.frame.size.width/8-7)];
    
    //Reset right truck
    [self.truckMover.rightTruck setFrame:CGRectMake(self.background.frame.size.width-self.background.frame.size.width*100/375, self.background.frame.size.height/2-self.background.frame.size.width/8+1,  self.background.frame.size.width*100/375, self.background.frame.size.width/8-7)];
    
    /*Reset top trucks drop*/
    
    //Reset top truck drop
    [self.truckMover.topTruck.drop removeFromSuperview];
    
    //Reset bottom truck drop
    [self.truckMover.bottomTruck.drop removeFromSuperview];
    
    //Reset left truck drop
    [self.truckMover.leftTruck.drop removeFromSuperview];
    
    //Reset right truck drop
    [self.truckMover.rightTruck.drop removeFromSuperview];
}

-(void)swipeDown:(UISwipeGestureRecognizer*)sgr {
    
    //Create a new view controller to display the information the user requested
    WeatherViewController* wvc = [WeatherViewController new];
    
    //Remove the truck mover from the superview so no other animation sequences are started and no other view controllers are created
    [self.truckMover removeFromSuperview];
    
    //Begin the animation sequence
    [UIView animateWithDuration:.7 delay:0 usingSpringWithDamping:.2 initialSpringVelocity:.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.truckMover.topTruck.frame = CGRectMake(self.background.frame.size.width/8*3 +5, self.background.frame.size.height/2, self.background.frame.size.width/8-7, self.background.frame.size.width*100/375);
    } completion:^(BOOL finished) {
        
        self.truckMover.topTruck.drop = [[UIImageView alloc]initWithFrame:CGRectMake(self.background.frame.size.width/16* 7, self.background.frame.size.height/2 - self.background.frame.size.width/16, 1, 1)];
        [self.truckMover.topTruck.drop setImage:[UIImage imageNamed:@"AppCloud"]];
        [self.background addSubview:self.truckMover.topTruck.drop];
        [UIView animateWithDuration:.2 animations:^{
            self.truckMover.topTruck.drop.frame = CGRectMake(self.background.frame.size.width/8* 3, self.background.frame.size.height/2-self.background.frame.size.width/8, self.background.frame.size.width/8, self.background.frame.size.width/8);
            
        }];
        
        [UIView animateWithDuration:.5 animations:^{
            self.truckMover.topTruck.frame = CGRectMake(self.background.frame.size.width/8*3 +5, self.background.frame.size.height/5 * 3, self.background.frame.size.width/8-7, self.background.frame.size.width*100/375);
                }completion:^(BOOL finished) {
                    
                    //Push the user requested view controller
                    [self.navigationController pushViewController:wvc animated:YES];
                }];
    }];
    
}

-(void)swipeUp:(UISwipeGestureRecognizer*)usgr{
    
    //Create a new view controller to display the information the user requested
    ArticleSearchViewController* asvc = [ArticleSearchViewController new];
    
    //Remove the truck mover from the superview so no other animation sequences are started and no other view controllers are created
    [self.truckMover removeFromSuperview];
    
    [UIView animateWithDuration:.7 delay:0 usingSpringWithDamping:.2 initialSpringVelocity:.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.truckMover.bottomTruck.frame = CGRectMake(self.background.frame.size.width/2 +5, self.background.frame.size.height/2-self.background.frame.size.width*100/375, self.background.frame.size.width/8-7, self.background.frame.size.width*100/375);
    } completion:^(BOOL finished) {
        
        self.truckMover.bottomTruck.drop = [[UIImageView alloc]initWithFrame:CGRectMake(self.background.frame.size.width/16* 9, self.background.frame.size.height/16*9, 1, 1)];
        [self.truckMover.bottomTruck.drop setImage:[UIImage imageNamed:@"AppCloud"]];
        [self.background addSubview:self.truckMover.bottomTruck.drop];
        [UIView animateWithDuration:.2 animations:^{
            self.truckMover.bottomTruck.drop.frame = CGRectMake(self.background.frame.size.width/2, self.background.frame.size.height/2, self.background.frame.size.width/8, self.background.frame.size.width/8);
        
        }];
        
        [UIView animateWithDuration:.5 animations:^{
            self.truckMover.bottomTruck.frame = CGRectMake(self.background.frame.size.width/2 +5, self.background.frame.size.height/5 * 2-self.background.frame.size.width*100/375, self.background.frame.size.width/8-7, self.background.frame.size.width*100/375);
        }completion:^(BOOL finished) {
                
                //Push the user requested view controller
                [self.navigationController pushViewController:asvc animated:YES];
        }];
    }];
    
    
}

-(void)swipeRight:(UISwipeGestureRecognizer*)rsgr{
    [UIView animateWithDuration:.7 delay:0 usingSpringWithDamping:.2 initialSpringVelocity:.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.truckMover.leftTruck.frame = CGRectMake(self.background.frame.size.width/2, self.background.frame.size.height/2+5, self.background.frame.size.width*100/375, self.background.frame.size.width/8-7);
    } completion:^(BOOL finished) {
        
        self.truckMover.leftTruck.drop = [[UIImageView alloc]initWithFrame:CGRectMake(self.background.frame.size.width/16* 7, self.background.frame.size.height/2+self.background.frame.size.height/16, 1, 1)];
        [self.truckMover.leftTruck.drop setImage:[UIImage imageNamed:@"AppCloud"]];
        [self.background addSubview:self.truckMover.leftTruck.drop];
        [UIView animateWithDuration:.2 animations:^{
            self.truckMover.leftTruck.drop.frame = CGRectMake(self.background.frame.size.width/8*3, self.background.frame.size.height/2, self.background.frame.size.width/8, self.background.frame.size.width/8);
            
        }];
        
        [UIView animateWithDuration:.5 animations:^{
            self.truckMover.leftTruck.frame = CGRectMake(self.background.frame.size.width/10*7, self.background.frame.size.height/2+5, self.background.frame.size.width*100/375, self.background.frame.size.width/8-7);
            
        }];
    }];
}

-(void)swipeLeft:(UISwipeGestureRecognizer*)rsgr{
    
    MovieInfoViewController* mivc = [MovieInfoViewController new];
    [self.truckMover removeFromSuperview];
    
    
    [UIView animateWithDuration:.7 delay:0 usingSpringWithDamping:.2 initialSpringVelocity:.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.truckMover.rightTruck.frame = CGRectMake(self.background.frame.size.width/2-self.background.frame.size.width*100/375, self.background.frame.size.height/2-self.background.frame.size.width/8+1,  self.background.frame.size.width*100/375, self.background.frame.size.width/8-7);
    } completion:^(BOOL finished) {
        
        self.truckMover.rightTruck.drop = [[UIImageView alloc]initWithFrame:CGRectMake(self.background.frame.size.width/16* 9, self.background.frame.size.height/2-self.background.frame.size.width/16, 1, 1)];
        [self.truckMover.rightTruck.drop setImage:[UIImage imageNamed:@"AppCloud"]];
        [self.background addSubview:self.truckMover.rightTruck.drop];
        [UIView animateWithDuration:.2 animations:^{
            self.truckMover.rightTruck.drop.frame = CGRectMake(self.background.frame.size.width/2, self.background.frame.size.height/2-self.background.frame.size.width/8, self.background.frame.size.width/8, self.background.frame.size.width/8);
            
        }];
        
        [UIView animateWithDuration:.5 animations:^{
            self.truckMover.rightTruck.frame = CGRectMake(self.background.frame.size.width*3/10-self.background.frame.size.width*100/375, self.background.frame.size.height/2-self.background.frame.size.width/8+1,  self.background.frame.size.width*100/375, self.background.frame.size.width/8-7);
        }completion:^(BOOL finished) {
            
            //Push the user requested view controller
            [self.navigationController pushViewController:mivc animated:YES];
        
        }];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
