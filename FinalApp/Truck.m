//
//  Truck.m
//  FinalApp
//
//  Created by Michelle Griffin on 7/20/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "Truck.h"

@implementation Truck

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setImage:[UIImage imageNamed:@"IosTruckTrans"]];
    self.txt = [[BorderedLabel alloc]init];
    
    self.drop = [[UIImageView alloc]init];
    
    return self;
}

-(instancetype)init
{
    self = [super init];
    [self setImage:[UIImage imageNamed:@"IosTruckTrans"]];
    self.txt = [[BorderedLabel alloc]init];
    
    self.drop = [[UIImageView alloc]init];
    
    return self;
}
    
@end
