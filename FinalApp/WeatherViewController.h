//
//  WeatherViewController.h
//  FinalApp
//
//  Created by Matthew Griffin on 7/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+MyRed.h"

@interface WeatherViewController : UIViewController


//Temperatures
@property(strong, nonatomic)NSNumber* currentTemp;
@property(strong, nonatomic)NSNumber* pHigh;
@property(strong, nonatomic)NSNumber* pLow;

//Labels
@property(strong, nonatomic)UILabel* currentLbl;
@property(strong, nonatomic)UILabel* hLbl;
@property(strong, nonatomic)UILabel* lLbl;

@end
