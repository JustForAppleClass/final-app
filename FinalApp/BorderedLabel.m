//
//  BorderedLabel.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/23/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "BorderedLabel.h"

@implementation BorderedLabel

-(instancetype)init{
    //Call super init
    self = [super init];
    
    //Add a new label in the middle of the original
    self.foreground = [[UILabel alloc]init];
    [self.foreground setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.foreground];
    self.foreground.textAlignment = NSTextAlignmentCenter;
    
    
    //Position foreground using autolayout
    NSDictionary *views = @{
                            @"fore":self.foreground,
                            };
    
    NSDictionary* metrics = @{
                              
                              };
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[fore]-2-|" options:0 metrics:metrics views:views];
    [self addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[fore]-2-|" options:0 metrics:metrics views:views];
    [self addConstraints:constraints];
    
    
    return self;
}

@end
