//
//  MovieInfoViewController.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/23/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "MovieInfoViewController.h"

@implementation MovieInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Set nav title
    self.navigationItem.title = @"Movie";
    
    //Set up movie title label
    self.movieTitle = [UILabel new];
    [self.movieTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.movieTitle.backgroundColor = [UIColor MyGreen];
    self.movieTitle.font = [UIFont fontWithName:@"Avenir Next" size:25 ];
    self.movieTitle.textColor = [UIColor whiteColor];
    [self.view addSubview:self.movieTitle];
    
    //Set up release date
    self.releaseDate = [UILabel new];
    [self.releaseDate setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.releaseDate.backgroundColor = [UIColor MyRed];
    self.releaseDate.font = [UIFont fontWithName:@"Avenir Next" size:20 ];
    self.releaseDate.textColor = [UIColor whiteColor];
    [self.view addSubview:self.releaseDate];
    
    //set up critic review score
    self.criticStars = [UILabel new];
    [self.criticStars setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.criticStars.backgroundColor = [UIColor MyBlue];
    self.criticStars.font = [UIFont fontWithName:@"Avenir Next" size:20 ];
    self.criticStars.textColor = [UIColor whiteColor];
    [self.view addSubview:self.criticStars];
    
    //Set up plot synopsis
    self.plot = [UITextView new];
    [self.plot setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.plot.backgroundColor = [UIColor MyLavender];
    [self.view addSubview:self.plot];
    
    //Create a small size for title elements
    NSNumber* small = [NSNumber numberWithDouble:self.view.frame.size.height/10];
    NSNumber* small_2 = [NSNumber numberWithDouble:self.view.frame.size.height/20];
    
    //Set up autolayout dictionaries
    NSDictionary *views = @{
                            @"title":self.movieTitle,
                            @"rlsD":self.releaseDate,
                            @"rev":self.criticStars,
                            @"plot":self.plot,
                            };
    
    NSDictionary* metrics = @{
                              @"small":small,
                              @"small_2":small_2,
                              };
    
    //Add the constraints to the view
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[title(small)][rlsD(small_2)][rev(small_2)][plot]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[title]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rlsD]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rev]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[plot]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.movieTitle.text = @"Star wars 7";
    self.releaseDate.text = @"Released 12/25/15";
    self.criticStars.text = @"Stars 2/5";
    self.plot.text = @"";
}

@end
