//
//  Truck.h
//  FinalApp
//
//  Created by Michelle Griffin on 7/20/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BorderedLabel.h"

@interface Truck : UIImageView

-(instancetype)initWithFrame:(CGRect)frame;
-(instancetype)init;

@property(strong, nonatomic)BorderedLabel* txt;
@property(strong, nonatomic)UIImageView* drop;

@end
