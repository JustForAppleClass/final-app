//
//  UIColor+MyRed.h
//  FinalApp
//
//  Created by Michelle Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MyRed)

+(UIColor*)MyRed;

+(UIColor*)MyGreen;

+(UIColor*)MyBlue;

+(UIColor*)MyYellow;

+(UIColor*)MyLavender;

@end
