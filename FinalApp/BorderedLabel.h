//
//  BorderedLabel.h
//  FinalApp
//
//  Created by Matthew Griffin on 7/23/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BorderedLabel : UIView

-(instancetype)init;

@property(strong,nonatomic)UILabel* foreground;

@end
