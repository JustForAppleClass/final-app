//
//  ArticleCell.h
//  FinalApp
//
//  Created by Michelle Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@property(strong,nonatomic)UILabel* headline;
@property(strong,nonatomic)UILabel* byline;

@end
