//
//  ViewController.h
//  FinalApp
//
//  Created by Matthew Griffin on 7/15/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TruckMover.h"
#import "Truck.h"
#import "WeatherViewController.h"
#import "ArticleSearchViewController.h"
#import "MovieInfoViewController.h"
#import "UIColor+MyRed.h"



@interface ViewController : UIViewController

@property(strong, nonatomic)TruckMover* truckMover;
@property(strong, nonatomic)UIView* background;



@end

