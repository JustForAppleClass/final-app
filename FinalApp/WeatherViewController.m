//
//  WeatherViewController.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "WeatherViewController.h"

@interface WeatherViewController ()

@end

@implementation WeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Set up the starting height for display purposes
    double startHeight = 0;
    
    //Set background color
    self.view.backgroundColor = [UIColor MyYellow];
    
    //Set up Imageview for weather icon
    UIImageView* weatherIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, startHeight, self.view.frame.size.width,self.view.frame.size.height/4)];
    [weatherIcon setImage:[UIImage imageNamed:@"AppCloud"]];
    weatherIcon.backgroundColor = [UIColor MyLavender];
    [self.view addSubview:weatherIcon];
    
    //Set navigation Title
    self.navigationItem.title = @"Weather";
    
    //Set up label for current temperature
    UILabel* cLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, startHeight+self.view.frame.size.height/4, self.view.frame.size.width/2, self.view.frame.size.height/8)];
    cLbl.backgroundColor= [UIColor MyGreen];
    cLbl.textColor = [UIColor whiteColor];
    cLbl.text = @"Currently:";
    cLbl.textAlignment = NSTextAlignmentRight;
    cLbl.font = [UIFont fontWithName:@"Avenir Next" size:30 ];
    [self.view addSubview:cLbl];
    
    //Position label for value of current temperature
    self.currentLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, startHeight+self.view.frame.size.height/4, self.view.frame.size.width/2, self.view.frame.size.height/8)];
    self.currentLbl.backgroundColor = [UIColor MyGreen];
    self.currentLbl.textColor = [UIColor whiteColor];
    self.currentLbl.font = [UIFont fontWithName:@"Avenir Next" size:30];
    [self.view addSubview:self.currentLbl];
    
    //Set up label for predicted High
    UILabel* hLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, startHeight+self.view.frame.size.height/8*3, self.view.frame.size.width/2, self.view.frame.size.height/8)];
    hLbl.backgroundColor = [UIColor MyRed];
    hLbl.text = @"High:";
    hLbl.font = [UIFont fontWithName:@"Avenir Next" size:30];
    hLbl.textAlignment = NSTextAlignmentRight;
    hLbl.textColor = [UIColor whiteColor];
    [self.view addSubview:hLbl];
    
    //Position label for value of predicted high
    self.hLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, startHeight+self.view.frame.size.height/8*3, self.view.frame.size.width/2, self.view.frame.size.height/8)];
    self.hLbl.font = [UIFont fontWithName:@"Avenir Next" size:30];
    self.hLbl.backgroundColor = [UIColor MyRed];
    self.hLbl.textColor = [UIColor whiteColor];
    [self.view addSubview:self.hLbl];
    
    //Set up label for predicted low
    UILabel* lLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, startHeight+self.view.frame.size.height/2, self.view.frame.size.width/2, self.view.frame.size.height/8)];
    lLbl.backgroundColor = [UIColor MyBlue];
    lLbl.text = @"Low:";
    lLbl.font = [UIFont fontWithName:@"Avenir Next" size:30];
    lLbl.textAlignment = NSTextAlignmentRight;
    lLbl.textColor = [UIColor whiteColor];
    [self.view addSubview:lLbl];
    
    //Position label for value of predicted low
    self.lLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, startHeight+self.view.frame.size.height/2, self.view.frame.size.width/2, self.view.frame.size.height/8)];
    self.lLbl.font = [UIFont fontWithName:@"Avenir Next" size:30];
    self.lLbl.backgroundColor = [UIColor MyBlue];
    self.lLbl.textColor = [UIColor whiteColor];
    [self.view addSubview:self.lLbl];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.currentTemp = [NSNumber numberWithDouble:45.6];
    self.currentLbl.text = [NSString stringWithFormat:@"%@",self.currentTemp ];
    self.pHigh = [NSNumber numberWithDouble:50.2];
    self.hLbl.text = [NSString stringWithFormat:@"%@",self.pHigh];
    self.pLow = [NSNumber numberWithDouble:40.1];
    self.lLbl.text = [NSString stringWithFormat:@"%@",self.pLow];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
