//
//  ArticleCell.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ArticleCell.h"

@implementation ArticleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    //Call the super function
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    
    //Set up the Headline for the article
    self.headline = [[UILabel alloc]init];
    [self.headline setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.headline.textColor = [UIColor whiteColor];
    self.headline.font = [UIFont fontWithName:@"Avenir Next" size:20 ];
    [self addSubview:self.headline];
    
    //Set up the byline for the article
    self.byline = [[UILabel alloc]init];
    [self.byline setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.byline.textColor = [UIColor grayColor];
    self.byline.textAlignment = NSTextAlignmentRight;
    self.byline.font = [UIFont fontWithName:@"Avenir Next" size:15 ];
    [self addSubview:self.byline];
    
    
    
    NSDictionary *views = @{
                            @"Head":self.headline,
                            @"by":self.byline,
                            };
    
    NSDictionary* metrics = @{
                              
                              };
    
    //set up and add autolayout constraints
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[Head][by]|" options:0 metrics:metrics views:views];
    [self addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[Head]|" options:0 metrics:metrics views:views];
    [self addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[by]|" options:0 metrics:metrics views:views];
    [self addConstraints:constraints];
    
    
    return self;
}

@end
