//
//  UIColor+MyRed.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "UIColor+MyRed.h"

@implementation UIColor (MyRed)

+(UIColor*) MyRed{
    return [UIColor colorWithRed:240.0/255.0 green:92.0/255.0 blue:56.0/255.0 alpha:1.0];
}
+(UIColor*) MyGreen{
    return [UIColor colorWithRed:151.0/255.0 green:199.0/255.0 blue:48.0/255.0 alpha:1.0];
}

+(UIColor*)MyBlue{
    return [UIColor colorWithRed:108.0/255.0 green:200.0/255.0 blue:215.0/255.0 alpha:1.0];

}

+(UIColor*)MyYellow{
    return [UIColor colorWithRed:244.0/255.0 green:254.0/255.0 blue:146.0/255.0 alpha:1.0];
    
}

+(UIColor*)MyLavender{
    return [UIColor colorWithRed:241.0/255.0 green:213.0/255.0 blue:244.0/255.0 alpha:1.0];
}

@end
