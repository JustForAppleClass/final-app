//
//  TruckMover.h
//  FinalApp
//
//  Created by Michelle Griffin on 7/20/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Truck.h"

@interface TruckMover : UIView

@property(strong,nonatomic)Truck* topTruck;
@property(strong,nonatomic)Truck* bottomTruck;
@property(strong,nonatomic)Truck* leftTruck;
@property(strong,nonatomic)Truck* rightTruck;

@end
