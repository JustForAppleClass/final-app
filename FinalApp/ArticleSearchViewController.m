//
//  ArticleSearchViewController.m
//  FinalApp
//
//  Created by Matthew Griffin on 7/22/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ArticleSearchViewController.h"

@implementation ArticleSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"News Articles";
    
    UITableView* articleTable = [[UITableView alloc]initWithFrame:self.view.bounds];
    articleTable.delegate = self;
    articleTable.dataSource = self;
    articleTable.backgroundColor = [UIColor MyYellow];
    [self.view addSubview:articleTable];
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[ArticleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    
    
    
    cell.headline.text = @"Something something tequila";
    cell.byline.text = @"An article by Jimmy Buffet";
    
    cell.backgroundColor = [UIColor MyBlue];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ArticleView* av = [ArticleView new];
    [self.navigationController pushViewController:av animated:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
